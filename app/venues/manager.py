from typing import List, Optional

from sqlalchemy import update
from sqlalchemy.engine import Result
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from app.venues.models import Venue
from app.venues.schema import VenueSchema


class VenueManager:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    async def create(self, venue: VenueSchema) -> Venue:
        new_venue = Venue(name=venue.name, type=venue.type)
        self.db_session.add(new_venue)
        await self.db_session.flush()
        return new_venue

    async def get(self, venue_id: int) -> Optional[Venue]:
        query = select(Venue).filter(Venue.id == venue_id)
        result: Result = await self.db_session.execute(query)
        return result.one_or_none()

    async def all(self, exclude: List[int] = []) -> List[Venue]:
        query = select(Venue)

        if len(exclude) > 0:
            query = query.filter(Venue.id.not_in(exclude))

        result: Result = await self.db_session.execute(query)
        return result.scalars().all()

    async def update(self, venue_id: int, venue: VenueSchema):
        data = venue.dict(exclude_unset=True)
        query = (
            update(Venue)
            .filter(Venue.id == venue_id)
            .returning(Venue)
            .values(**data)
        )
        result = await self.db_session.execute(query)
        return result.one_or_none()
