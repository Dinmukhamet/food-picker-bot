from datetime import datetime

from sqlalchemy import Column, DateTime, Enum, Integer, String

from app.settings import Base
from app.types import VenueType


class Venue(Base):
    __tablename__ = "venues"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    type = Column(Enum(VenueType))

    created_at = Column(DateTime(timezone=True), default=datetime.utcnow)
    updated_at = Column(DateTime(timezone=True), onupdate=datetime.utcnow)
