from pydantic import BaseModel

from app.types import VenueType


class VenueSchema(BaseModel):
    name: str
    type: VenueType
