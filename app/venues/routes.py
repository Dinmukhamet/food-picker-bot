import random
from typing import List, Optional

from fastapi import APIRouter, Depends, Query, status
from fastapi.responses import JSONResponse

from app.dependencies import get_venue_manager
from app.venues.manager import VenueManager
from app.venues.schema import VenueSchema

router = APIRouter()


@router.post("/venues/")
async def create_venue(
    venue: VenueSchema,
    venue_manager: VenueManager = Depends(get_venue_manager),
):
    return await venue_manager.create(venue)


@router.get("/venues/")
async def get_all_venues(
    venue_manager: VenueManager = Depends(get_venue_manager),
) -> List[VenueSchema]:
    return await venue_manager.all()


@router.get("/venues/{venue_id}/")
async def get_venue(
    venue_id: int, venue_manager: VenueManager = Depends(get_venue_manager)
) -> VenueSchema:
    venue = await venue_manager.get(venue_id)
    return venue[0]


@router.put(
    "/venues/{venue_id}/",
)
async def update_venue(
    venue_id: int,
    venue: VenueSchema,
    venue_manager: VenueManager = Depends(get_venue_manager),
):
    result = await venue_manager.update(venue_id, venue)
    return result


@router.get("/random/")
async def get_random_venue(
    exclude: Optional[List[int]] = Query([]),
    venue_manager: VenueManager = Depends(get_venue_manager),
):
    venue_list = await venue_manager.all(exclude)
    if not venue_list:
        return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST)
    return random.choice(venue_list)
