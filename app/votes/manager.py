from datetime import date
from typing import Optional

from fastapi import HTTPException
from sqlalchemy import delete, func, update
from sqlalchemy.engine import Result
from sqlalchemy.future import select
from sqlalchemy.orm import Session
from sqlalchemy.sql import exists

from app.types import ActionType
from app.votes.models import Vote
from app.votes.schema import VoteSchema


class VoteManager:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    async def create(self, vote: VoteSchema):
        query = (
            exists(Vote)
            .where(
                Vote.user_id == vote.user_id,
                Vote.action == vote.action,
                Vote.venue_id == vote.venue_id,
                func.DATE(Vote.created_at) == date.today(),
            )
            .select()
        )

        result: Result = await self.db_session.execute(query)
        if result.scalar():
            raise HTTPException(
                status_code=400,
                detail="User can't vote twice for the same option",
                headers={"Content-Type": "application/json"},
            )

        stmt = delete(Vote).where(
            Vote.user_id == vote.user_id,
            Vote.venue_id == vote.venue_id,
            func.DATE(Vote.created_at) == date.today(),
        )
        await self.db_session.execute(
            stmt, execution_options={"synchronize_session": "fetch"}
        )

        new_vote = Vote(
            user_id=vote.user_id, venue_id=vote.venue_id, action=vote.action
        )
        self.db_session.add(new_vote)
        await self.db_session.commit()
        return new_vote

    async def get(self, vote_id: int) -> Optional[Vote]:
        query = select(Vote).filter(Vote.id == vote_id)
        result: Result = await self.db_session.execute(query)
        return result.one_or_none()

    async def all(self, date: Optional[date] = None):
        query = select(Vote)
        result: Result = await self.db_session.execute(
            query if date is None else query.filter(Vote.created_at == date)
        )
        return result.scalars().all()

    async def update(self, vote_id: int, vote: VoteSchema):
        data = vote.dict(exclude_unset=True)
        query = (
            update(Vote)
            .filter(Vote.id == vote_id)
            .returning(Vote)
            .values(**data)
        )
        result: Result = await self.db_session.execute(query)
        return result.one_or_none()

    async def votes_info(self, venue_id: int):
        query = select(
            func.count(Vote.id)
            .filter(Vote.action == ActionType.up)
            .label("up"),
            func.count(Vote.id)
            .filter(Vote.action == ActionType.down)
            .label("down"),
        ).filter(
            Vote.venue_id == venue_id,
            func.DATE(Vote.created_at) == date.today(),
        )
        result: Result = await self.db_session.execute(query)
        return result.one()
