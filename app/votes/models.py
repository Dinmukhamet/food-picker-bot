from datetime import datetime

from app.settings import Base
from app.types import ActionType
from sqlalchemy import BigInteger, Column, DateTime, Enum, ForeignKey, Integer


class Vote(Base):
    __tablename__ = "votes"

    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger)
    venue_id = Column(Integer, ForeignKey("venues.id"))
    action = Column(Enum(ActionType))

    created_at = Column(DateTime(timezone=True), default=datetime.utcnow)
    updated_at = Column(DateTime(timezone=True), onupdate=datetime.utcnow)
