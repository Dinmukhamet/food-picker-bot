from pydantic import BaseModel

from app.types import ActionType


class VoteSchema(BaseModel):
    user_id: int
    venue_id: int
    action: ActionType
