from datetime import date
from typing import List, Optional

from app.dependencies import get_vote_manager
from app.votes.manager import VoteManager
from app.votes.schema import VoteSchema
from fastapi import APIRouter, Depends

router = APIRouter()


@router.post("/votes/")
async def create_vote(
    vote: VoteSchema, vote_manager: VoteManager = Depends(get_vote_manager)
):
    return await vote_manager.create(vote=vote)


@router.get("/votes/")
async def get_all_votes(
    date: Optional[date] = None,
    vote_manager: VoteManager = Depends(get_vote_manager),
) -> List[VoteSchema]:
    return await vote_manager.all(date=date)


@router.get("/votes/{venue_id}/")
async def get_votes_info(
    venue_id: int, vote_manager: VoteManager = Depends(get_vote_manager)
):
    return await vote_manager.votes_info(venue_id)
