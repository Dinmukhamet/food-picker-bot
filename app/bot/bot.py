import os
import sys
from pathlib import Path
from typing import Dict

import aiogram.utils.markdown as md
import aiohttp
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.callback_data import CallbackData

sys.path.insert(1, os.path.abspath("."))
sys.path.insert(
    1, Path(os.path.dirname(os.path.realpath(__file__))).parent.parent
)


from app.bot.utils import check  # noqa
from app.settings import Settings  # noqa

storage = MemoryStorage()
settings = Settings()
bot = Bot(token=settings.api_token)
dp = Dispatcher(bot, storage=storage)
vote_cb = CallbackData("vote", "venue", "action")  # post:<venue>:<action>
venue_type_cb = CallbackData("venue", "type")
confirm_cb = CallbackData("confirm", "option")


class VenueForm(StatesGroup):
    type = State()
    name = State()
    is_confirmed = State()


@dp.message_handler(commands=["start", "help"])
async def send_welcome(message: types.Message):
    await message.reply("Привет, я предлагаю места, где можно поесть 🍽")


@dp.message_handler(commands=["add_new_venue"])
async def add_new_venue(message: types.Message):
    await VenueForm.type.set()

    on_site = types.InlineKeyboardButton(
        text="Отправиться в заведение",
        callback_data=venue_type_cb.new(type="on_site"),
    )

    order = types.InlineKeyboardButton(
        text="Взять на заказ", callback_data=venue_type_cb.new(type="order")
    )

    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(on_site)
    keyboard.add(order)

    await bot.send_message(
        chat_id=message.chat.id,
        text=md.text("Выберите тип."),
        reply_markup=keyboard,
    )


@dp.callback_query_handler(
    venue_type_cb.filter(type=["on_site", "order"]), state=VenueForm.type
)
async def get_type_of_venue(
    query: types.CallbackQuery, callback_data: Dict, state: FSMContext
):
    async with state.proxy() as data:
        data["type"] = callback_data.get("type")
    await VenueForm.next()
    await bot.send_message(
        chat_id=query.message.chat.id, text="А теперь отправьте мне название"
    )
    await query.message.delete()


@dp.message_handler(state=VenueForm.name)
async def get_name_of_venue(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["name"] = message.text

        markup = types.InlineKeyboardMarkup()

        no = types.InlineKeyboardButton(
            text="Нет", callback_data=confirm_cb.new(option="no")
        )

        yes = types.InlineKeyboardButton(
            text="Да", callback_data=confirm_cb.new(option="yes")
        )

        markup.row(no, yes)

        await VenueForm.next()

        await bot.send_message(
            chat_id=message.chat.id,
            text=md.text("Создаю новое заведение:", md.bold(data["name"])),
            reply_markup=markup,
            parse_mode=types.ParseMode.MARKDOWN,
        )


@dp.callback_query_handler(
    confirm_cb.filter(option=["yes", "no"]), state=VenueForm.is_confirmed
)
async def confirm_venue_creation(
    query: types.CallbackQuery, callback_data: Dict, state: FSMContext
):
    if callback_data.get("option") == "yes":
        async with state.proxy() as data:
            body = {"type": data["type"], "name": data["name"]}
            async with aiohttp.ClientSession(
                headers={"Content-Type": "application/json"}
            ) as session:
                async with session.post(
                    f"{settings.api_host}/venues/",
                    json=body,
                ) as response:
                    if response.status == 200:
                        await query.answer(text="Заведение создано 🎉")
                    else:
                        await query.answer(text="Что-то пошло не так 🤷‍♂️")
    else:
        await query.answer(text="Начните заново!")
    await query.message.delete()


@dp.callback_query_handler(vote_cb.filter(action=["up", "down"]))
async def vote_cb_handler(query: types.CallbackQuery, callback_data: Dict):
    actions = {"up": 1, "down": 0}
    venue_id = callback_data.get("venue")

    body = {
        "user_id": query.from_user.id,
        "venue_id": venue_id,
        "action": actions.get(callback_data.get("action")),
    }

    headers = {"Content-Type": "application/json"}
    async with aiohttp.ClientSession(headers=headers) as session:
        members_count = await query.message.chat.get_member_count()
        async with session.post(
            f"{settings.api_host}/votes/", json=body
        ) as response:
            if response.status == 400:
                result = await response.json()
                await query.answer(result.get("detail"))

        await check(
            bot, query.message, session, venue_id, members_count, vote_cb
        )


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
