import random
from typing import Dict, Optional


class Message:
    MESSAGES = [
        "Сегодня отличный день, чтобы",
        "Как на счет того, чтобы",
        "Предлагаю сегодня",
        "Судьба велит вам",
    ]

    OPTIONS = {"on_site": "пойти в", "order": "заказать из"}

    def __init__(self, data: Dict, is_reselected: bool = False):
        self.venue = data
        self.is_reselected = is_reselected

    @property
    def start(self) -> str:
        if self.is_reselected:
            return "Что ж, а что на счет"
        return random.choice(self.MESSAGES)

    @property
    def middle(self) -> Optional[str]:
        try:
            return self.OPTIONS[self.venue.get("type")]
        except KeyError:
            pass

    @property
    def end(self) -> str:
        return self.venue.get("name")

    def __str__(self):
        """
        Function to get message
        dependig on type of venue:
        - on_site: пойти в
        - order: заказать из
        """

        if self.is_reselected:
            return f"{self.start} {self.end}?"
        return f"{self.start} {self.middle} {self.end}"
