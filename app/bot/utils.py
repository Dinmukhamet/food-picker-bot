from typing import List

import aiohttp
from aiogram import Bot, types
from aiogram.utils.callback_data import CallbackData

from app.bot.types import Message
from app.settings import settings


def get_keyboard(
    venue_id: int, vote_cb: CallbackData
) -> types.InlineKeyboardMarkup:
    like = types.InlineKeyboardButton(
        text="👍", callback_data=vote_cb.new(venue=venue_id, action="up")
    )
    dislike = types.InlineKeyboardButton(
        text="👎", callback_data=vote_cb.new(venue=venue_id, action="down")
    )

    return types.InlineKeyboardMarkup(inline_keyboard=[[like, dislike]])


async def check(
    bot: Bot,
    message: types.Message,
    session: aiohttp.ClientSession,
    venue_id: int,
    chat_members_count: int,
    vote_cb: CallbackData,
):
    async with session.get(
        f"{settings.api_host}/votes/{venue_id}"
    ) as response:
        data = await response.json()

        up = data.get("up")
        down = data.get("down")

        # if number of people who voted agains given venue is greater
        # than number of people who voted for this venue
        # and it's greater than the half of chat members number
        # than suggest one more venue excluding given one
        if down > up and down >= chat_members_count / 2:
            return await suggest(bot, session, vote_cb, exclude=[venue_id])
        elif up > down and up >= chat_members_count / 2:
            async with session.get(
                f"{settings.api_host}/venues/{venue_id}/"
            ) as response:
                venue = await response.json()
                await message.edit_text(
                    text=f"Отлично, общим голосванием выбрано заведение \"{venue.get('name')}\"", # noqa
                    reply_markup=None,
                )


async def suggest(
    bot: Bot,
    session: aiohttp.ClientSession,
    vote_cb: CallbackData,
    exclude: List[int] = [],
    is_reselected: bool = False,
):
    url = f"{settings.api_host}/random/"

    params = [("exclude", value) for value in exclude]
    async with session.get(url, params=params) as response:
        data = await response.json()
        message = "Что-то пошло не так 🤷‍♂️"
        markup = None
        if response.status == 200:
            message = Message(data, is_reselected=is_reselected)
            markup = get_keyboard(data.get("id"), vote_cb)

        await bot.send_message(
            chat_id=settings.chat_id, text=message, reply_markup=markup
        )
