import aiohttp
from arq import cron
from arq.connections import RedisSettings

from app.bot.bot import bot, vote_cb
from app.bot.utils import suggest
from app.settings import settings


async def send_info(*args, **kwargs):
    async with aiohttp.ClientSession() as session:
        await suggest(bot, session, vote_cb)


class WorkerSettings:
    redis_settings = RedisSettings(host=settings.redis_host)
    cron_jobs = [cron(send_info, second=0)]
