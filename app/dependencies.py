from app.settings import async_session
from app.venues.manager import VenueManager
from app.votes.manager import VoteManager


async def get_venue_manager():
    async with async_session() as session:
        async with session.begin():
            yield VenueManager(session)


async def get_vote_manager():
    async with async_session() as session:
        async with session.begin():
            yield VoteManager(session)
