from fastapi import FastAPI

from app.venues.routes import router as venues_router
from app.votes.routes import router as votes_router

app = FastAPI()
app.include_router(venues_router)
app.include_router(votes_router)
