import enum


class ActionType(int, enum.Enum):
    down = 0
    up = 1


class VenueType(str, enum.Enum):
    on_site = "on_site"
    order = "order"
