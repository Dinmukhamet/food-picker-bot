from pydantic import BaseSettings
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker


class Settings(BaseSettings):
    database_url: str
    api_token: str
    api_host: str = "http://localhost:8000"
    chat_id: int = -663193039
    redis_host: str = "localhost"


settings = Settings()
engine = create_async_engine(settings.database_url, future=True, echo=True)
async_session = sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession
)
Base = declarative_base()
