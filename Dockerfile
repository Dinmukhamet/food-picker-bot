FROM python:3.8-slim

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  build-essential \
  gcc


COPY Pipfile Pipfile.lock ./
RUN pip install pip --upgrade
RUN pip install pipenv --upgrade
RUN pipenv install --deploy --ignore-pipfile --dev

ENV TZ=Asia/Bishkek
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY . .
COPY start-api.sh .